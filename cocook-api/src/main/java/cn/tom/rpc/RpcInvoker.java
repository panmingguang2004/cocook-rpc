package cn.tom.rpc;

import java.io.IOException;

import io.zbus.mq.Broker;
import io.zbus.mq.Consumer;
import io.zbus.mq.ConsumerConfig;
import io.zbus.mq.Protocol;

public class RpcInvoker {

	private Broker broker;
	private String topic;
	private String serviceAddress;
	private RpcProcessor rpcProcessor;
	
	public RpcInvoker() {
	}
	
	public RpcInvoker(Broker broker, String topic, RpcProcessor rpcProcessor) {
		this.broker = broker;
		this.topic = topic;
		this.rpcProcessor = rpcProcessor;
	}

	public RpcInvoker(String serviceAddress, String topic, RpcProcessor rpcProcessor) {
		this(new Broker(serviceAddress), topic, rpcProcessor);
	}

	public void join() throws IOException, InterruptedException {
		ConsumerConfig config = new ConsumerConfig(broker);
		config.setTopicMask(Protocol.MASK_RPC | Protocol.MASK_MEMORY);
		
		config.setTopic(topic);
		config.setConnectionCount(1);      // connection count to each server in broker
		config.setConsumeRunnerPoolSize(64);  // consume handler running thread pool size
		config.setMessageHandler(rpcProcessor);
		
		Consumer consumer = new Consumer(config);
		consumer.start(); 
	}
	

	public Broker getBroker() {
		return broker;
	}

	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	public RpcProcessor getRpcProcessor() {
		return rpcProcessor;
	}

	public void setRpcProcessor(RpcProcessor rpcProcessor) {
		this.rpcProcessor = rpcProcessor;
	}

	public String getServiceAddress() {
		return serviceAddress;
	}

	public void setServiceAddress(String serviceAddress) {
		this.serviceAddress = serviceAddress;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
}
