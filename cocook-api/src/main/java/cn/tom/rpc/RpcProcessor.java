package cn.tom.rpc;

import java.io.IOException;
import java.util.Map;


import cn.tom.kit.Logkit;
import io.zbus.mq.Message;
import io.zbus.mq.MessageHandler;
import io.zbus.mq.MqClient;
import tom.kit.Multimap;
import tom.kit.json.JsonUtil;
import tom.net.Session;
import tom.net.http.HttpMessage;

public class RpcProcessor implements MessageHandler, tom.net.http.MessageHandler{
	
	private  ModuleInvoker moduleInvoker;
	

	public RpcProcessor(ModuleInvoker moduleInvoker) {		
		this.moduleInvoker = moduleInvoker;
	}
	
	@SuppressWarnings("unchecked")
	private  Message process(Message msg){
		long a  = System.currentTimeMillis();
		String body = msg.getBodyString();
		// 直接body 内容为json字符串
		Map<String, Object> params = null;
		Object respBody = null;
		try{
			params = (Map<String, Object>) JsonUtil.deserialize(body);
			respBody  = moduleInvoker.invoke(params);
		}catch(Exception e){
			Logkit.error("RpcProcessor.process::" , e);
			respBody = Multimap.createMap("state", "500").set("msg", e.getMessage());
		}
		
		msg.setBody(JsonUtil.serializeToBytes(respBody));
		Logkit.info(params.get("module") + " -> " + (System.currentTimeMillis()-a));
		return msg;
	}
	
	
	@Override
	public void handle(Message msg, MqClient client) throws IOException {
		final String sender = msg.getSender();
		Message res = process(msg);
		
		if(res != null){
			res.setReceiver(sender);  
			client.route(res);
		}
		
	}
	
	@Override
	public void handleMessage(HttpMessage msg, Session sess) throws IOException {
		HttpMessage res = process2(msg);
		res.setStatus("200");
		sess.write(res);
	}
	
	
	@SuppressWarnings("unchecked")
	private  HttpMessage process2(HttpMessage msg){
		long a  = System.currentTimeMillis();
		String body = msg.getBodyString();
		// 直接body 内容为json字符串
		Map<String, Object> params = null;
		Object respBody = null;
		try{
			params = (Map<String, Object>) JsonUtil.deserialize(body);
			respBody  = moduleInvoker.invoke(params);
		}catch(Exception e){
			Logkit.error("RpcProcessor.process::" , e);
			respBody = Multimap.createMap("state", "500").set("msg", e.getMessage());
		}
		
		msg.setBody(JsonUtil.serializeToBytes(respBody));
		Logkit.info(params.get("module") + " -> " + (System.currentTimeMillis()-a));
		return msg;
	}
	
	

}
