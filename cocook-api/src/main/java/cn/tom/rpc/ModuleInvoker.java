package cn.tom.rpc;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import cn.tom.kit.Logkit;
import cn.tom.service.BaseService;
import tom.cocook.ext.BeanFactory;
import tom.cocook.ext.Scan;
import tom.kit.Multimap;

/**
 * 记录 了所有的 调用的 module[method] 和 service[class实例]
 * @author tomsun
 */

public class ModuleInvoker {
	
	private static final HashMap<String, ServiceHandler> services = new HashMap<String, ServiceHandler>();
	
	public ModuleInvoker() {
	}
	
	public ModuleInvoker(String packageName) throws Exception {
		scanpackage(packageName);
	}
	
	protected Object invoke(Map<String, Object> map) {
		Multimap<String, String> error =  Multimap.createMap("state", "500");
		String module = (String) map.get("module");
		try {
			if (module == null) {
				return error.set("msg", "module["+module+"] is null");
			}
			
			String m[] = module.split("\\.");
			ModuleInvoker.ServiceHandler handler = services.get(m[0]);
			if (handler == null) {
				return error.set("msg", "service:[" + m[0] + "] is null");
			}
			Method meth = handler.getMethod(m[1]);
			if (meth == null) {
				return error.set("msg", "method:[" + m[1] + "] is null");
			}
			
			Object obj = meth.invoke(handler.getService(), map);
			return obj;
		} catch (Exception e) {
			Logkit.error("ModuleInvoker.invoker", e);
			return  error.set("msg", "异常请求, 重试或检查网络");
		}
	};

	protected Object invokeAll(Map<String, Object> map) {
		HashMap<String, Object> mm = new HashMap<>();
		String modules = (String) map.remove("modules");
		for (String module : modules.split("\\|")) {
			map.put("module", module);
			Object obj = invoke(map);
			mm.put(module.split("\\.")[1], obj);
		}
		return mm;
	}
	
	/**
	 * 包扫描 
	 * @param _packagename
	 * @throws Exception
	 */
	public void scanpackage(String packageName) throws Exception {
		Set<Class<?>> set = Scan.scanpackage(packageName);
		for(Class<?> clazz : set){
			if(BaseService.class.isAssignableFrom(clazz)){ //继承 BaseService 的判断 做完Rpc service
				services.put(clazz.getSimpleName(), new ServiceHandler(clazz));
			}
		}
		System.out.println("scanService::"+services);
	}
	
	public static <T> T getService(Class<T> clazz){
		return clazz.cast(services.get(clazz.getSimpleName()).getService());
	}
	

	public static class ServiceHandler {
		private BaseService baseService;
		private HashMap<String, Method> methods = new HashMap<>();

		public Method getMethod(String key) {
			return methods.get(key);
		}

		public BaseService getService() {
			return baseService;
		}

		public ServiceHandler(Class<?> _class) {
			baseService = (BaseService) BeanFactory.newInstance(_class);
			searchMethod(_class);
		}

		private HashMap<String, Method> searchMethod(Class<?> clazz) {
			/**
			 * 获取 @Path
			 */
			while (clazz != Object.class) {
				Method[] meths = clazz.getDeclaredMethods();
				for (Method me : meths) {
					methods.put(me.getName(), me);
				}
				clazz = clazz.getSuperclass();
			}
			return methods;
		}
	}

}
