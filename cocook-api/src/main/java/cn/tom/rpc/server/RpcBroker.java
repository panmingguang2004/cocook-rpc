package cn.tom.rpc.server;

import io.zbus.mq.server.MqServer;
import io.zbus.mq.server.MqServerConfig;

/**
 * 服务中间件,使用MQ类型的RPC 
 * @author tomsun
 */
public class RpcBroker {
	public  static  MqServer  start() throws Exception {
		
		MqServerConfig mqServerConfig = new MqServerConfig();
		mqServerConfig.setServerPort(15555);
//		mqServerConfig.addTracker("127.0.0.1:15556");  // HA服务端
		MqServer server = new MqServer(mqServerConfig);
		
		server.start();
		
		return server;
		
//		mqServerConfig = new MqServerConfig();
//		mqServerConfig.setServerPort(15556);
//		mqServerConfig.addTracker("127.0.0.1:15555");  // HA服务端
//		MqServer server2 = new MqServer(mqServerConfig);
//		
//		server2.start();
		
	}
	
	
	public static void main(String[] args) throws Exception {
		RpcBroker.start();
	}
}
