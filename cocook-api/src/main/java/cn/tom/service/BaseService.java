package cn.tom.service;

import java.io.IOException;
import java.util.Properties;

import tom.db.jdbc.simple.DBUtil;
import tom.kit.io.PropertiesUtil;
import tom.kit.io.Resource;

public class BaseService {

	static {
		// new DefaultLogConfigure().config();
		Resource resource = new Resource("DBConfig.properties");
		Properties props = null;
		try {
			props = PropertiesUtil.loadProperties(resource);
		} catch (IOException e) {
			e.printStackTrace();
		}
		DBUtil.init(props);

	}
}
