package cn.tom;

import cn.tom.rpc.ModuleInvoker;
import cn.tom.rpc.RpcInvoker;
import cn.tom.rpc.RpcProcessor;
import cn.tom.rpc.server.RpcBroker;
import io.zbus.mq.Broker;
import io.zbus.mq.server.MqServer;

public class RpcServiceRun {

	public static void main(String[] args) throws Exception {
		MqServer server = RpcBroker.start();

		ModuleInvoker moduleInvoker = new ModuleInvoker("cn.tom.service");
		RpcProcessor rpcProcessor = new RpcProcessor(moduleInvoker);
		String serviceName = RpcServiceRun.class.getSimpleName();
		 //  new Broker(server)         
		RpcInvoker rpcService = new RpcInvoker("127.0.0.1:15555" , serviceName, rpcProcessor);
		rpcService.join();
	}

}
