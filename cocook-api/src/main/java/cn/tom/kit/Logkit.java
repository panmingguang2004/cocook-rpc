package cn.tom.kit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Logkit {
	
	public static Logger logger = LoggerFactory.getLogger("CommLog");
	
	public static void info(String msg) {
		logger.info(msg);
	}

	public static void info(String msg, Throwable e) {
		logger.info(msg, e);
	}

	public static void error(String msg, Throwable e) {
		logger.error(msg, e);
	}
	
	
}
