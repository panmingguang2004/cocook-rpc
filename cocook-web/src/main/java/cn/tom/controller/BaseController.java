package cn.tom.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cn.tom.rpc.RpcClientInvoker;
import tom.cocook.core.ControllerModel;
import tom.cocook.core.RequestContext;

public class BaseController extends ControllerModel {

	@Override
	public boolean before(RequestContext context, Map<String, String> map) {
		return super.before(context, map);
	}
	
	static RpcClientInvoker rpcInvoker = new RpcClientInvoker("127.0.0.1:15555");
	static{
		try {
			rpcInvoker.addTopic("UserService", "RpcServiceRun");
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected Object invoke(Map<String, String> map) {
		Object obj = null;
		try { 
			obj = rpcInvoker.invoke(map);
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}

		return obj;
	}

	protected Object invokeAll(Map<String, String> map) {
		HashMap<String, Object> mm = new HashMap<>();
		String modules = map.remove("modules");
		for (String module : modules.split("\\|")) {
			map.put("module", module);
			Object obj = invoke(map);
			mm.put(module.split("\\.")[1], obj);
		}
		return mm;
	}

}
