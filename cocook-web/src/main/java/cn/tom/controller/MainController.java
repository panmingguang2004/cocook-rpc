package cn.tom.controller;

import java.util.Map;

import tom.cocook.annotation.Handler.Controller;
import tom.cocook.annotation.Handler.Path;
import tom.cocook.annotation.HttpMethod;
import tom.cocook.annotation.Response.Json;

@Controller
public class MainController extends BaseController{

	
	@Json
	@Path("/get")
	public Object get(Map<String, String> map) {
		
		return invoke(map);
	}
	
	@Json
	@Path(value = "/post", method = HttpMethod.POST)
	public Object post(Map<String, String> map) {
		return invoke(map);
	}
	
	@Json
	@Path("/batch")
	public Object batch(Map<String, String> map) {
		return invokeAll(map);
	}
	
	
}
