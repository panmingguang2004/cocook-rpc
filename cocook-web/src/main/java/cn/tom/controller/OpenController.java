package cn.tom.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;

import tom.cocook.annotation.Handler.Controller;
import tom.cocook.annotation.Handler.Path;
import tom.cocook.annotation.HttpMethod;
import tom.cocook.annotation.Response.Json;
import tom.cocook.core.RequestContext;

@Controller
public class OpenController extends BaseController{

	@Json
	@Path("/open/get")
	public Object get(Map<String, String> map) {
		return invoke(map);
	}
	
	@Json
	@Path("/open/batch")
	public Object batch(Map<String, String> map) {
		return invokeAll(map);
	}

	@Json
	@Path(value = "/open/post", method = HttpMethod.POST)
	public Object post(Map<String, String> map) {
		return invoke(map);
	}

	@Override
	public boolean before(RequestContext context, Map<String, String> map) {
		
		// TODO 直接按照拦截 serviceName 拦截 , 只通过open的
		
		
		// 支付宝 和 微信支付回调 无法检查 token, 所以取消验证 checkAnent(context, map);
		Cookie agent = context.getCookie("agent");
		if(agent!=null){
			map.put("agent", agent.getValue());
		}
		return true;
	}
	
	boolean checkAnent(RequestContext context, Map<String, String> map) throws IOException {
		Cookie agent = context.getCookie("agent"); // 验证agent 的正确性
		if (agent == null || !("Web".equals(agent.getValue()) || "Ios".equals(agent.getValue()) || "And".equals(agent.getValue()))) {
			context.printJSON("{\"state\":\"503\",\"msg\":\"agent设置不对\"}");
			return false;
		}
		map.put("agent", agent.getValue());
		return true;
	}

	protected Object invoke(Map<String, String> map) {
		try {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			HashMap<String, Object> mm =  new HashMap<String, Object>();
			mm.put("state", "500");
			mm.put("msg", "连接异常, 重试或检查网络");
			return  mm;
		}
	};
	
	protected Object invokeAll(Map<String, String> map) {
		HashMap<String, Object> mm = new HashMap<>();
		String modules = map.remove("modules");
		for(String module: modules.split("\\|")){
			map.put("module", module);
			Object obj = invoke(map);
			mm.put(module.split("\\.")[1], obj);
		}
		return mm;
	}
	
}
