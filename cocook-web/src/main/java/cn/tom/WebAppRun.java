package cn.tom;

import cn.tom.listener.WebEmbedListener;
import tom.cocook.UndertowStart;

public class WebAppRun {
	
	public static void main(String[] args) throws Exception {
		new UndertowStart(WebAppRun.class).init(args)
		.addCocookServlet()
		//.addServlet(statViewServlet).addFilter(webStatFilter)
		.addlistener(WebEmbedListener.class)
		.start();
	}

}
