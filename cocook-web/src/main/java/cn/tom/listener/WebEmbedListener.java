package cn.tom.listener;

import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

import cn.tom.rpc.RpcClientInvoker;
import tom.cocook.config.Constants;
import tom.cocook.core.CocookListener;
/**
 * 回收内存, 防止内存泄漏, 尤其是 连接池泄漏
 * 内存重启jvm 就能解决, 连接池需要重启数据库, 或者pgsql oracle可以直接杀进程
 * @author tomsun
 */
@WebListener
public class WebEmbedListener extends CocookListener{

	@Override
	protected void contextInit(ServletContextEvent arg0) {

		
		System.out.println("........contextInit ........");
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		super.contextDestroyed(arg0);
		
		//TODO 释放内存, pool, 连接池 线程池 等
		
		System.out.println("........contextDestroyed ........");
	}

}
