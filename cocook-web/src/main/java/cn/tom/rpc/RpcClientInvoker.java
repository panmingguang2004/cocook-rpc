package cn.tom.rpc;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.zbus.mq.Broker;
import io.zbus.mq.Message;
import io.zbus.mq.Producer;
import io.zbus.mq.ProducerConfig;
import io.zbus.mq.Protocol;
import io.zbus.mq.Protocol.TopicInfo;
import tom.kit.ObjectId;
import tom.kit.json.JsonUtil;
import tom.net.aio.AioClient;
import tom.net.http.HttpMessage;
import tom.net.http.MessageAdaptorClient;

public class RpcClientInvoker {
	
	private Broker broker;
	private String serviceAddress;
	private ProducerConfig producerConfig = new ProducerConfig();
	private Producer producer;
	
	// service - > topic 
	private ConcurrentHashMap<String, String> serviceTopic = new ConcurrentHashMap<>(100);
	
	public RpcClientInvoker() {
		
	}

	public RpcClientInvoker(String serviceAddress) {
		this(new Broker(serviceAddress));
	}
	
	public RpcClientInvoker(Broker broker) {
		this.broker = broker;
		toRpcproducer();
	}
	
	public void toRpcproducer(){
		producerConfig.setBroker(broker);
		producerConfig.setTopicMask(Protocol.MASK_RPC | Protocol.MASK_MEMORY);
		producer = new Producer(producerConfig);
	}
	
	public TopicInfo[] addTopic(String serviceName, String topic) throws IOException, InterruptedException {
		serviceTopic.put(serviceName, topic);
		return producer.declareTopic(serviceName);
	}

	public Object invoke(Map<String, String> params) throws IOException, InterruptedException{
		String module = params.get("module");
		Message msg = new Message();
		msg.setId(Long.toString(new ObjectId().toLong()));
		String serviceName = module.substring(0, module.indexOf("."));
		msg.setTopic(serviceTopic.get(serviceName));
		msg.setAck(false);
		msg.setBody(JsonUtil.serializeToBytes(params));
		Message res = producer.publish(msg);
		Object obj = JsonUtil.deserialize(res.getBodyString());
		return  obj;
	}
	
	
	public Broker getBroker() {
		return broker;
	}

	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	public String getServiceAddress() {
		return serviceAddress;
	}

	public void setServiceAddress(String servceAddress) {
		this.serviceAddress = servceAddress;
	}

	public ProducerConfig getProducerConfig() {
		return producerConfig;
	}

	public void setProducerConfig(ProducerConfig producerConfig) {
		this.producerConfig = producerConfig;
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}

	public ConcurrentHashMap<String, String> getServiceTopic() {
		return serviceTopic;
	}

	
}
